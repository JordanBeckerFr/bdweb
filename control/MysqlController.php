<?php

namespace control;

use \model\Family;
use \model\Article;


class MysqlController extends AbstractController {

    public $app;

    public function __construct($app) {
        parent::__construct($app->request);
        $this->app=$app;
    }

    // Used when calling /mysql/ route
    public function home() {
        $t1 = microtime(true);
        $id = 32130205;
        $current = Family::with('articles')->find($id);
        $families=$this->query($current);

        $this->displayNav($families, $id);
        $this->displayArticles($current);

        $t = microtime(true) - $t1;
        echo "<div style='color:red;'>Page générée en $t sec.</div>";
    }

    // Used when calling /mysql/id route
    public function explore($id) {
        $t1 = microtime(true);
        $current = Family::with('articles')->find($id);

        $families=$this->query($current);

        $this->displayNav($families, $id);
        $this->displayArticles($current);
        $t = microtime(true) - $t1;
        echo "<div style='color:red;'>Page générée en $t sec.</div>";

    }

    /**
     * @param $id int, the currently selected object id
     * @return array, result of the query
     */
    private function query($current) {



        $parents = explode(';', $current->parents);
        //Current ID is a parent for someone else too!
        $parents[] = $current->id;

        /**
         * THE MAGIC HAPPENS HERE
         * Basically: we gather every family whose id_parent is in current family's parents list. We order by depth,
         * so we can easily "split" the array into multiple columns in the view.
         *
         * We only want depth > 1 because depth 1 only has one family, the whole catalogue.
         *
         * We order by depth DESCENDING because we want to have the last array first, to avoid
         * having to traverse the resulting array backwards (more handy for backtracking each parents).
         *
         * The second order by is only to have each depth alphabetically ordered. Just UX thing.
         */
        return Family::where('depth', '>', 1)->whereIn('id_parent', $parents)->orderBy('depth', 'desc')->orderBy('label', 'asc')->get();
    }


    /**
     * @param $families array, result of the query
     * @param $parent int, the first parent ID to highlight
     */
    private function displayNav($families, $parent) {
        echo "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/></head><body>";

        // Preparing strucutre for display, one array per depth, minus the depth 1
        $array=array();
        $array[2]=array();
        $array[3]=array();
        $array[4]=array();
        $array[5]=array();
        $array[6]=array();

        $localparent=$parent;

        foreach ($families as $family) {
            $clabel=$family['attributes']['label'];
            $cid=$family['attributes']['id'];
            $cdepth=$family['attributes']['depth'];
            $isparent=false;
            if ($localparent == $cid) {
                $isparent=true;
                $localparent=$family['attributes']['id_parent'];
            }


            $array[$cdepth][]=array('id'=>$cid, 'label'=>$clabel, 'isparent' => $isparent);

        }

        for ($i = 2; $i<=6; $i++) {
            echo "<div style='display: inline-block; width: 20%; vertical-align: top;'>";

            foreach ($array[$i] as $entry) {
                $clabel=$entry['label'];
                $url=$this->app->urlFor('explore', array(
                    'id' => $entry['id']));

                if ($entry['isparent']) {
                    echo "<a style='display: block; background-color: orange;' href='".$url."'>$clabel</a>";
                }
                else {
                    echo "<a style='display: block;' href='".$url."'>$clabel</a>";
                }

            }

            echo "</div>";

        }
    }


    private function displayArticles($current) {
        $articles = $current->articles;

        foreach ($articles as $article) {
            $label=$article['label'];
            echo "<p>$label</p>";
        }
        echo "</body>";

    }
}