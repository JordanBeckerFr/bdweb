<?php

namespace control;


class MongoController extends AbstractController {

    private $db;

    public function __construct($app) {
        parent::__construct($app->request);
        $this->app=$app;
        $client = \config\MongoConnection::getConnection("mongo.ini");
        $this->db=$client->catalogue;
        $this->families = $this->db->families;
        $this->articles = $this->db->articles;
    }
    public function home() {
        $t1 = microtime(true);
        $db = $this->db;
        $db->selectCollection('families');
        $families = $db->families;
        $records = $families->findOne(array('depth'=>1));

        $this->displayNav($records);
        $t = microtime(true) - $t1;
        echo "<div style='color:red;'>Page générée en $t sec.</div>";
    }

    public function explore($id){
        $t1 = microtime(true);
        $family = $this->families->findOne(array('id'=>intval($id)));
        $this->displayNav($family);
        $t = microtime(true) - $t1;
        echo "<div style='color:red;'>Page générée en $t sec.</div>";

    }



    private function displayNav($family) {

        echo "<!DOCTYPE html><html><head><meta charset=\"utf-8\"/></head><body>";
        //Tableau final par depth
        $array = array();
        $array[2] = array();
        $array[3] = array();
        $array[4] = array();
        $array[5] = array();
        $array[6] = array();
        $articles = array();

        $depth = $family['depth'];
        $id = $family['id'];

        $array[$depth][$family['id']] = $family;
        $array[$depth][$family['id']]['color'] = 'red';

        $parents = array();
        foreach ($family['parents'] as $p) {
            $parents[$p['id']] = intval($p['id']);
        }

        //Childs
        foreach ($family['childs'] as $child) {
            if($child['depth']==$depth+1){
                $array[$child['depth']][$child['id']] = $child;
                $array[$child['depth']][$child['id']]['color'] = 'white';
            }
        }

        foreach ($family['articles'] as $article) {
            $articles[$article['id']] = $article;
        }


        $families = $this->families->find(array('id'=>array('$in'=>$parents)));
        foreach ($families as $key => $parent) {
            $array[$parent['depth']][$parent['id']] = $parent;
            foreach ($parent['childs'] as $pchild) {
                if($pchild['depth'] == $parent['depth']+1){
                    $array[$pchild['depth']][$pchild['id']] = $pchild;
                }
            }
            if(isset($parents['articles'])){
                foreach ($parents['articles'] as $article) {
                    $articles[$article['id']] = $article;
                }
            }

        }

        for ($i = 2; $i < 7; $i++) {
            echo "<div style='display: inline-block;width: 20%;vertical-align: top;'>";
            foreach ($array[$i] as $entry) {
                if($entry['id']==$id){
                    $color = 'red';
                }else{
                    if(array_key_exists($entry['id'],$parents)){
                        $color = 'orange';
                    } else {
                        $color = 'white';
                    }
                }
                $url = $this->app->urlFor('exploreMongo',array('id'=>$entry['id']));
                $label = $entry['label'];
                $depth = $entry['depth'];
                
                echo "<a style='display:block; background-color: $color' href='$url'>$label - $depth</a>";

            }
            echo "</div>";
        }
        echo "</div>";
        echo "<div style='display: block'><ul>";
        foreach ($articles as $article) {
            $alabel = $article['label'];
            echo "<li>$alabel</li>";
        }

        echo "</ul></div>";


    }

}