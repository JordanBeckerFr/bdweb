<?php
namespace control;

use Slim\Http\Request;
use Slim\Slim;


abstract class AbstractController {

    public $request;

    public function __construct(Request $request) {
        $this->request = $request;
        $this->app = Slim::getInstance();
    }
}