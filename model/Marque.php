<?php

namespace model;

use \Illuminate\Database\Eloquent\Model;

class Marque extends Model {

	protected $table = 'marque';

	public function articles() {
		return $this->hasMany('\model\Article', 'id_marque');
	}
}