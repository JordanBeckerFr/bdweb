<?php

namespace model;

use \Illuminate\Database\Eloquent\Model;

class Article extends Model {

	protected $table = 'article';

	public function marque() {
		return $this->belongsTo('\model\Marque', 'id_marque');
	}

	public function families() {
		return $this->belongsToMany('\model\Family', 'family_article', 'id_article', 'id_family');
	}
}