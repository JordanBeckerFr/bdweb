<?php

namespace model;

use \Illuminate\Database\Eloquent\Model;

class Family extends Model {

	protected $table="family";

	public function articles() {
		return $this->belongsToMany('model\Article', 'family_article', 'id_family', 'id_article');
	}
}