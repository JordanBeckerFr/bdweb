<?php
require 'vendor/autoload.php';



$app=new \Slim\Slim();
\config\MysqlConnection::connection("mysql.ini");

$app->get('/', function() use ($app) {
    $controller = new \control\IndexController($app->request());
    $controller->home($app);
});

$app->group('/mysql', function() use ($app) {
	$app->get('/', function () use ($app){
		$controller=new \control\MysqlController($app);
		$controller->home();
	})->name('mysql');

	$app->get('/:id', function($id) use ($app) {
		$controller=new \control\MysqlController($app);
		$controller->explore($id);
	})->name('explore');
});

$app->group('/mongo', function() use ($app) {
	$app->get('/', function () use ($app){
		$controller=new \control\MongoController($app);
		$controller->home();

	})->name('mongo');
    $app->get('/:id',function($id) use ($app) {
        $controller = new \control\MongoController($app);
        $controller->explore($id);
    })->name('exploreMongo');
});

$app->run();