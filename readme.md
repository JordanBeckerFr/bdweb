# BD Web
> Comparaison MySQL / MongoDB

## Présentation

Cette mini application a pour but de fournir une navigation dans des familles de produits.

Afin de prendre mesure des forces/faiblesses des SGBD, nous avons programmé une version utilisant MySQL et une utilisant MongoDB.

Elles devraient être identiques, mais pas au niveau des performances.

À nous de trouver les algorithmes !

## Humans

- Jordan Becker
- Julien Michel

## Robots

- Tars

## Licence

MIT