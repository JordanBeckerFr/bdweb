<?php

namespace config;


class MongoConnection {

    public static function getConnection($filename) {
        $params=parse_ini_file($filename);
        if ($params['url'] == '') {
            return new \MongoClient();
        }
        else {
            return new \MongoClient($params["url"], array(
                'username' => $params['username'],
                'password' => $params['password'],
                'db' => $params['db']));
        }


    }

}