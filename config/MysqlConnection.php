<?php

namespace config;
use \Illuminate\Database\Capsule\Manager as DB;

class MysqlConnection {
    public static function connection($filename) {
        $db = new DB();
        $ini_array = parse_ini_file($filename);

        if (!$ini_array) throw new \Exception("Unable to load $filename");

        $db->addConnection($ini_array);
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}